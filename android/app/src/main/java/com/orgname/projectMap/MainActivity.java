package com.orgname.projectMap;

import android.content.Intent;
import com.facebook.react.ReactActivity;
import cl.json.RNSharePackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.evollu.react.fa.FIRAnalyticsPackage;
import io.invertase.firebase.RNFirebasePackage;
import com.evollu.react.fcm.FIRMessagingPackage;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "orgname.projectMap";
    }

    @Override
    public void onNewIntent (Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
   } 
}
