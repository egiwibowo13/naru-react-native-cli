import React from "react";
import { Image, View, FlatList, Dimensions, AsyncStorage, StyleSheet, TouchableOpacity, ActivityIndicator, TouchableHighlight } from "react-native";
import {
  Container,
  Header,
  Content,
  Left,
  Right,
  Body,
  Title,
  Button,
  Icon,
  Item,
  Input,
  Card,
  CardItem,
  Footer,
  FooterTab,
  Text
} from "native-base";
import { AppHeader, AppFooter } from "../app-nav/index"
import ListItem from "../promo/ListItem.js";

const ITEM_WIDTH = Dimensions.get('window').width;
var Analytics = require('react-native-firebase-analytics');

export default class Promo extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      promo: [],
      isReady: false,
      coloums: 2
    }
  }

  componentWillMount() {
    Analytics.logEvent('promo', {
      'item_id': 'promo'
    });
  }


  componentDidMount() {
    //no18 id detail masih salah
    AsyncStorage.getItem("bumnId", (error, result) => {
      fetch("http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/listpromos/", {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      })
        .then(response => response.json())
        .then((data) => {
          if (data.length > 0) {
            this.setState({ promo: data });
          }
          this.setState({ isReady: true });
        })
    })
  }
  render() {
    const coloums = this.state.coloums
    let prevItem = {};
    if (!this.state.isReady) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )
    }
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title="Promo" />
        <Content>
          <FlatList style={{ marginTop: "5%" }}
            numColumns={coloums}
            data={this.state.promo}
            renderItem={({ item }) => {
              return (
                <TouchableOpacity style={styles.container} onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: item._id })}>
                  <ListItem itemWidth={(ITEM_WIDTH - 20) / coloums} image={item.listNewsPromoImage[0]} />
                </TouchableOpacity>
              )
            }}
            keyExtractor={(index) => {
              return index
            }} />
        </Content>
      </Container>
    );
  }

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  loading: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

