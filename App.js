import React, { Component } from "react";
import { View, Image, StyleSheet, AsyncStorage, ActivityIndicator } from "react-native";
import { Container, Content, Picker, Button, Text } from "native-base";
import Intro from "./src/screen/index.js";
import Sett from "./src/screen/sett.js";
import Navbar from "./src/navbar/index.js";

import { Platform } from 'react-native';
import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from 'react-native-fcm';

let date = new Date();
let d = date.getDate();
let m = date.getMonth() + 1;
let y = date.getFullYear();




AsyncStorage.getItem("subuh", (error, result) => {
  if (result == undefined) {

  } else {
    let subuh = new Date().setHours(result.slice(0,2),result.slice(3,5));
    let idSubuh = "1";
    let bodySubuh = "Saatnya Sholat Subuh";
    scheduleLocalNotification(subuh, idSubuh, bodySubuh);
    if (date>subuh) {
      FCM.cancelAllLocalNotifications();
    }
  }
});

AsyncStorage.getItem("dzuhur", (error, result) => {
  if (result == undefined) {

  } else {
    let dzuhur = new Date().setHours(result.slice(0,2),result.slice(3,5));
    let idDzuhur = "2";
    let bodyDzuhur = "Saatnya Sholat Dzuhur";
    scheduleLocalNotification(dzuhur, idDzuhur, bodyDzuhur);
    if (date>dzuhur) {
      FCM.cancelAllLocalNotifications();
    }
  }
});

AsyncStorage.getItem("ashar", (error, result) => {
  if (result == undefined) {

  } else {
    let ashar = new Date().setHours(result.slice(0,2),result.slice(3,5));
    let idAshar = "3";
    let bodyAshar = "Saatnya Sholat Ashar";
    scheduleLocalNotification(ashar, idAshar, bodyAshar);
    if (date>ashar) {
      FCM.cancelAllLocalNotifications();
    }
  }
});

AsyncStorage.getItem("maghrib", (error, result) => {
  if (result == undefined) {

  } else {
    let maghrib = new Date().setHours(result.slice(0,2),result.slice(3,5));
    let idMaghrib = "4";
    let bodyMaghrib = "Saatnya Sholat Maghrib";
    scheduleLocalNotification(maghrib, idMaghrib, bodyMaghrib);
    if (date>maghrib) {
      FCM.cancelAllLocalNotifications();
    }
  }
});

AsyncStorage.getItem("isya", (error, result) => {
  if (result == undefined) {

  } else {
    let isya = new Date().setHours(result.slice(0,2),result.slice(3,5));
    let idIsya = "5";
    let bodyIsya = "Saatnya Sholat Isya";
    scheduleLocalNotification(isya, idIsya, bodyIsya);
    if (date>isya) {
      FCM.cancelAllLocalNotifications();
    }
  }
});

FCM.getScheduledLocalNotifications().then(notif => console.log(notif));

// this shall be called regardless of app state: running, background or not running. Won't be called when app is killed by user in iOS
FCM.on(FCMEvent.Notification, async (notif) => {
  // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
  if (notif.local_notification) {
    //this is a local notification
  }
  if (notif.opened_from_tray) {
    //iOS: app is open/resumed because user clicked banner
    //Android: app is open/resumed because user clicked banner or tapped app icon
    console.log(notif);
  }
  // await someAsyncCall();

  if (Platform.OS === 'ios') {
    //optional
    //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see https://developer.apple.com/documentation/uikit/uiapplicationdelegate/1623013-application.
    //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
    //notif._notificationType is available for iOS platfrom
    switch (notif._notificationType) {
      case NotificationType.Remote:
        notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
        break;
      case NotificationType.NotificationResponse:
        notif.finish();
        break;
      case NotificationType.WillPresent:
        notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
        break;
    }
  }
});
FCM.on(FCMEvent.RefreshToken, (token) => {
  console.log(token)
  // fcm token may not be available on first load, catch it here
});




export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      error: null

    };
  }

  componentWillMount() {
    AsyncStorage.getItem("Splash", (error, result) => {
      this.setState({ result: result })
    })
  }

  componentWillUnmount() {
    // stop listening for events
    this.notificationListener.remove();
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        AsyncStorage.setItem("latitude", JSON.stringify(position.coords.latitude));
        AsyncStorage.setItem("longitude", JSON.stringify(position.coords.longitude));
        this.setState({
          error: null,
          isReady: true
        });
      });

    // iOS: show permission prompt for the first call. later just check permission in user settings
    // Android: check permission in user settings
    FCM.requestPermissions().then(() => console.log('granted')).catch(() => console.log('notification permission rejected'));

    FCM.getFCMToken().then(token => {
      console.log(token)
      // store fcm token in your server
    });

    this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
      // optional, do some component related stuff
    });

    // initial notification contains the notification that launchs the app. If user launchs app by clicking banner, the banner notification info will be here rather than through FCM.on event
    // sometimes Android kills activity when app goes to background, and when resume it broadcasts notification before JS is run. You can use FCM.getInitialNotification() to capture those missed events.
    // initial notification will be triggered all the time even when open app by icon so send some action identifier when you send notification
    FCM.getInitialNotification().then(notif => {
      console.log(notif)
    });
    FCM.subscribeToTopic('/topics/news');
  }

  render() {
    switch (this.state.result) {
      case undefined:
        return (
          <View style={styles.container}>
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
        );

      case null:
        return (
          <Intro />
        );

      default:
        return (
          <Sett />
        );
    }

  }
}

function scheduleLocalNotification(date,id,body) {
  FCM.scheduleLocalNotification({
    fire_date: date,           //RN's converter is used, accept epoch time and whatever that converter supports
    id: id,                              //REQUIRED! this is what you use to lookup and delete notification. In android notification with same ID will override each other
    body: body,
    large_icon: "ic_notif",               // Android only
    icon: "ic_notif",                     // as FCM payload, you can relace this with custom icon you put in mipmap
    color: "red",                        // Android only
    vibrate: 300,                        // Android only default: 300, no vibration if you pass 0
    wake_screen: true,
    repeat_interval: "day", //day, hour
    sound: "adzan.mp3",                   // as FCM payload
    priority: "high",
    show_in_foreground: true
  });
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
